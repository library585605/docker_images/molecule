# Custom docker image Molecule 24.2.1

URL: registry.gitlab.com/library585605/docker_images/molecule:latest

Версии ПО в образе:  
molecule 24.2.1    
python 3.11    
ansible:2.16.6  
docker:2.1.0   

`Для запуска контейнера нужно вначале создать структуру и сценарий для молекулы 24.`

- Можно скачать говотовый: [molecule_scenario_1](https://gitlab.com/library585605/docker_images/molecule_scenario_1.git) 

Как в ручную создать на хосте структуру для молекулы v24: 

1. Создаем структуру папок:   
`
mkdir ~/molecule/ && cd ~/molecule/`   
`mkdir -p collections/ansible_collections && cd ./collections/ansible_collections
`

2. Создаем namespace и collection:  
`ansible-galaxy collection init ganebalden.coll_molecule`

3. Добавляем playbooks extensions:   
`cd ./ganebalden/coll_molecule && mkdir ./playbooks && mkdir extensions`

4. Создаем сценарий:   
`cd ./extensions &&  molecule init scenario -d docker`   

5. Помещаем роль в папку:
`/collections/ansible_collections/ganebalden/coll_molecule/roles/`

Коректируем файлы или заменяем: molecule.yml prepare.yml converge.yml


 # Script:  
- Название контейнера
  `name_conteiner="molecule_24"`  

 - Путь к extensions
  `dst_path_workdir="/molecule/collections/ansible_collections/ganebalden/coll_molecule/extensions"`

 - Подключаем директорую к контейнеру:
  `host_volume_path="~/molecule/"`

- Докер образ:
  `docker_image="registry.gitlab.com/library585605/docker_images/molecule:latest"`



# Запуск

```
sudo docker run --rm -it --privileged --name $name_conteiner -d \ -e DOCKER_TLS_CERTDIR=/certs \ -w $dst_path_workdir \ -v "$host_volume_path":/molecule/:ro $docker_image molecule test

-- troubleshooting --
docker exec -it $name_conteiner /bin/sh

$ molecule converge

$ molecule destroy

docker stop molecule_24
```

Molecule_Runner
```
[[runners]]
  name = "Molecule_Run"
  url = "https://gitlab.com"
  id = 
  token = "glrt-"
  token_obtained_at = 
  token_expires_at = 
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "ubuntu:focal"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/certs/client"]
    shm_size = 0
    network_mtu = 0
 ```
- template_converge_file.sh - скрипт создает файл converge.yml
и менятет в конфиге название под текущую роль. Нужно для gitlab-ci.
