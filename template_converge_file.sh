#!/bin/sh
echo "---
- name: Converge
  hosts: all
  become: true
  tasks:
    - name: "Include roles"
      include_role:
        name: "$name_role"
" | tee "$path_extensions/molecule/default/converge.yml"
