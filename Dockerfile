FROM docker:26-dind

RUN apk update && mkdir -p /molecule/collections/ansible_collections && \
    apk add git py3-pip python3-dev rsync openssl-dev libffi-dev gcc libc-dev make && \
    pip3 install --upgrade pip --break-system-packages && \
    pip3 install --upgrade setuptools --break-system-packages && \
    pip3 install --no-cache-dir --upgrade git+https://github.com/ansible-community/molecule --break-system-packages && \
    pip3 install --no-cache-dir requests pytest-testinfra "molecule[docker]" molecule-docker --break-system-packages && \
    pip3 install --no-cache-dir --upgrade molecule==24.2.1 --break-system-packages
COPY ./molecule.yml ./prepare.yml ./template_converge_file.sh /molecule/
   